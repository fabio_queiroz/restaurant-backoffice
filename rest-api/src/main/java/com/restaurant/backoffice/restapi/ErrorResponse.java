package com.restaurant.backoffice.restapi;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

/**
 * Represents an error
 */
@ApiModel(
        value="ErrorResponse",
        description="Error response model"
)
@Builder
@Getter
public class ErrorResponse {
    /** Error timestamp */
    @ApiModelProperty("Error timestamp")
    private final Instant timestamp;
    /** Error message */
    @ApiModelProperty("Error message")
    private final String errorMessage;
}
