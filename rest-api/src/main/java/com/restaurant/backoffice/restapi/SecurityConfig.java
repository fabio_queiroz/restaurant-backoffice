package com.restaurant.backoffice.restapi;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * Security Configuration
 */
@Configuration
public class SecurityConfig {

    /** username */
    private final String username;
    /** password */
    private final String password;

    /**
     * Constructor
     *
     * @param username Username
     * @param password Password
     */
    public SecurityConfig(
            @Value("${backoffice.user.name}") String username,
            @Value("${backoffice.user.password}") String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Actuator Web Security configuration adapter
     */
    @Configuration
    @Order(1)
    public static class ActuatorWebSecurityConfigurationAdapter extends WebSecurityConfigurerAdapter {
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/**").hasRole("USER")
                    .and().httpBasic()
                    .and().csrf().disable();
        }
    }

    /**
     * Define PasswordEncoder bean
     *
     * @return PasswordEncoder bean
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    /**
     * Define UserDetailsService bean
     *
     * @param encoder Encoder
     * @return UserDetailsService bean
     */
    @Bean
    public UserDetailsService userDetailsService(PasswordEncoder encoder) {
        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
        manager.createUser(User.withUsername(username)
                .password(encoder.encode(password))
                .roles("USER").build());
        return manager;
    }

}
