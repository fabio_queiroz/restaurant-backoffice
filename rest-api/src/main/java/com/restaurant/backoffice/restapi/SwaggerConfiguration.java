package com.restaurant.backoffice.restapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

/**
 * Swagger configuration class
 */
@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    /**
     * Creating docket bean
     *
     * @return Docket bean
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }

    /**
     * Creating ApiInfo bean
     *
     * @return ApiInfo
     */
    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Restaurant Backoffice Rest API",
                "This API provides REST services to manage Restaurants",
                "1.0.0",
                "Terms of service...",
                new Contact(
                        "Restaurant Backoffice",
                        "www.restaurant-backoffice.com.br",
                        "contact@restaurant-backoffice.com"
                ),
                "License of API", "API license URL", Collections.emptyList());
    }

}