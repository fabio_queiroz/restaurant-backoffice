package com.restaurant.backoffice.restapi;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItem;
import com.restaurant.backoffice.datastore.entity.Restaurant;
import com.restaurant.backoffice.datastore.repository.MenuItemRepository;
import com.restaurant.backoffice.datastore.repository.RestaurantRepository;
import com.restaurant.backoffice.restapi.exception.EntityNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * This controller implements REST endpoints to create, update and delete Restaurants.
 * Create, update and delete Menu Items of a Restaurant.
 */
@Api(
        value = "Restaurant's Api for clients",
        description = "This controller implements REST endpoints to create, update and delete Restaurants" +
                ". Also creates, updates and deletes Menu Items of a Restaurant."
    )
@RestController
@RequestMapping("restaurants")
public class RestaurantController {

    private final RestaurantRepository restaurantRepository;
    private final MenuItemRepository menuItemRepository;

    public RestaurantController(
            RestaurantRepository restaurantRepository,
            MenuItemRepository menuItemRepository) {
        this.restaurantRepository = restaurantRepository;
        this.menuItemRepository = menuItemRepository;
    }

    /**
     * Get restaurant by id
     *
     * @param id Restaurant id
     * @return Restaurant
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Get restaurant by id",
            response = Restaurant.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Restaurant.class),
            @ApiResponse(code = 404, message = "Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @GetMapping("/{id}")
    public Restaurant findRestaurantById(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long id) throws EntityNotFoundException {
        return validateRestaurantExists(id);
    }

    /**
     * Get all restaurants
     *
     * @return List of Restaurant
     */
    @ApiOperation(
            value = "Get all restaurants",
            notes = "Result list is sorted by name",
            response = Restaurant.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Restaurant.class, responseContainer = "List"),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @GetMapping
    public List<Restaurant> findAllRestaurants() {
        return restaurantRepository.findAll(StructuredQuery.OrderBy.asc("name"));
    }

    /**
     * Create restaurant
     *
     * @param restaurant Restaurant object
     * @return Created restaurant with generated id
     */
    @ApiOperation(
            value = "Create a restaurant",
            notes = "Returns created model with generated id",
            response = Restaurant.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Restaurant.class),
            @ApiResponse(code = 400, message = "Restaurant name is required", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @PostMapping
    public ResponseEntity<Restaurant> createRestaurant(
            @ApiParam(value = "Restaurant model", required = true)
            @RequestBody @Validated Restaurant restaurant) {
        long id = restaurantRepository.save(
                Restaurant.builder()
                        .name(restaurant.getName())
                        .build()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                restaurantRepository.findById(id).get()
        );
    }

    /**
     * Update a restaurant
     *
     * @param id Restaurant id
     * @param restaurant Restaurant object
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Update a restaurant"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Restaurant name is required", response = ErrorResponse.class),
            @ApiResponse(code = 404, message = "Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @PutMapping("/{id}")
    public void updateRestaurant(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long id,
            @ApiParam(value = "Restaurant model", required = true)
            @RequestBody @Validated Restaurant restaurant) throws EntityNotFoundException {
        validateRestaurantExists(id);
        restaurantRepository.save(
                Restaurant.builder()
                .id(id)
                .name(restaurant.getName()).build()
        );
    }

    /**
     * Delete a restaurant by id
     *
     * @param id Restaurant id
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Delete a restaurant by id"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @DeleteMapping("/{id}")
    public void deleteRestaurant(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long id) throws EntityNotFoundException {
        validateRestaurantExists(id);
        restaurantRepository.delete(id);
    }

    /**
     * Get Menu Item by id and Restaurant id
     *
     * @param restaurantId Restaurant id
     * @param id Menu Item id
     * @return Menu Item
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Get Menu Item by id and Restaurant id",
            response = MenuItem.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = MenuItem.class),
            @ApiResponse(code = 404, message = "MenuItem or Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @GetMapping("/{restaurantId}/menuitems/{id}")
    public MenuItem findMenuItemByIdAndRestaurantId(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long restaurantId,
            @ApiParam(value = "Menu Item id", required = true)
            @PathVariable Long id) throws EntityNotFoundException {
        validateRestaurantExists(restaurantId);
        return validateMenuItemExists(id, restaurantId);
    }

    /**
     * Get all Menu Items of a Restaurant
     *
     * @param restaurantId Restaurant id
     * @return List of Menu Items
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Get all Menu Items of a Restaurant",
            response = MenuItem.class,
            responseContainer = "List"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = MenuItem.class, responseContainer = "List"),
            @ApiResponse(code = 404, message = "Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @GetMapping("/{restaurantId}/menuitems")
    public List<MenuItem> findAllMenuItemsByRestaurantId(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long restaurantId) throws EntityNotFoundException {
        validateRestaurantExists(restaurantId);
        return menuItemRepository.findAllByRestaurantId(restaurantId, StructuredQuery.OrderBy.asc("name"));
    }

    /**
     * Create Menu Item for a Restaurant
     *
     * @param restaurantId Restaurant id
     * @param menuItem Menu Item
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Create Menu Item for a Restaurant",
            notes = "Returns created model with generated id",
            response = MenuItem.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Menu Item created", response = MenuItem.class),
            @ApiResponse(code = 404, message = "Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @PostMapping("/{restaurantId}/menuitems")
    public ResponseEntity<MenuItem> createMenuItem(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long restaurantId,
            @ApiParam(value = "Menu Item model", required = true)
            @RequestBody @Validated MenuItem menuItem)
            throws EntityNotFoundException {
        validateRestaurantExists(restaurantId);
        long menuItemId = menuItemRepository.save(
                MenuItem.builder()
                    .name(menuItem.getName())
                    .price(menuItem.getPrice())
                    .restaurantId(restaurantId)
                    .build()
        );
        return ResponseEntity.status(HttpStatus.CREATED).body(
                menuItemRepository.findById(menuItemId).get()
        );
    }

    /**
     * Update Menu Item by id and Restaurant id
     *
     * @param restaurantId Restaurant id
     * @param id Menu Item id
     * @return Menu Item
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Update Menu Item by id and Restaurant id"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = MenuItem.class),
            @ApiResponse(code = 404, message = "MenuItem or Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @PutMapping("/{restaurantId}/menuitems/{id}")
    public void updateMenuItem(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long restaurantId,
            @ApiParam(value = "Menu Item id", required = true)
            @PathVariable Long id,
            @ApiParam(value = "Menu Item model", required = true)
            @RequestBody @Validated MenuItem menuItem) throws EntityNotFoundException {
        validateRestaurantExists(restaurantId);
        validateMenuItemExists(id, restaurantId);
        menuItemRepository.save(
                MenuItem.builder()
                .id(id)
                .name(menuItem.getName())
                .price(menuItem.getPrice())
                .restaurantId(restaurantId)
                .build()
        );
    }

    /**
     * Delete a Menu Item of a Restaurant by id
     *
     * @param restaurantId Restaurant id
     * @param id Menu Item id
     * @throws EntityNotFoundException
     */
    @ApiOperation(
            value = "Delete a Menu Item of a Restaurant by id"
    )
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "MenuItem or Restaurant not found", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Error processing request", response = ErrorResponse.class)
    })
    @DeleteMapping("/{restaurantId}/menuitems/{id}")
    public void deleteMenuItem(
            @ApiParam(value = "Restaurant id", required = true)
            @PathVariable Long restaurantId,
            @ApiParam(value = "Menu Item id", required = true)
            @PathVariable Long id) throws EntityNotFoundException {
        validateRestaurantExists(restaurantId);
        validateMenuItemExists(id, restaurantId);
        menuItemRepository.delete(id);
    }

    /**
     * Validate whether a Restaurant exists
     * @param id Restaurant id
     * @return Restaurant
     * @throws EntityNotFoundException
     */
    private Restaurant validateRestaurantExists(Long id) throws EntityNotFoundException{
        return restaurantRepository.findById(id).orElseThrow(
                () -> new EntityNotFoundException(
                        String.format("Restaurant %s not found", id)));
    }

    /**
     * Validate whether a Menu Item exists and it belongs to this Restaurant
     *
     * @param id Menu Item id
     * @param restaurantId Restaurant id
     * @return Menu Item entity
     * @throws EntityNotFoundException
     */
    private MenuItem validateMenuItemExists(Long id, Long restaurantId) throws EntityNotFoundException {
        Optional<MenuItem> menuItemOpt = menuItemRepository.findById(id);
        if (!(menuItemOpt.isPresent() && menuItemOpt.get().getRestaurantId().equals(restaurantId))) {
            throw new EntityNotFoundException(
                    String.format("Menu Item %s not found for Restaurant %s", id, restaurantId));
        }
        return menuItemOpt.get();
    }

}