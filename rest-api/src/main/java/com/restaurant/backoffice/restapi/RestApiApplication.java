package com.restaurant.backoffice.restapi;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

/**
 * Spring Boot Application entry-point
 */
@SpringBootApplication
@EnableEncryptableProperties
@ComponentScan("com.restaurant.backoffice")
@PropertySource("classpath:secure.yml")
public class RestApiApplication {

	/**
	 * Main method
     *
	 * @param args Arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(RestApiApplication.class, args);
	}

}
