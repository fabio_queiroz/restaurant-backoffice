package com.restaurant.backoffice.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Invalid schedule exception
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidScheduleException extends RestApiBaseException {

	public InvalidScheduleException(String message) {
		super(message);
	}

}