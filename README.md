# Restaurant Backoffice

This project is Proof of Concept for exploring Google Datastore. 
To achieve this goal a Spring Boot standalone application was designed and developed to be hosted on Google App Engine Flexible Environment.   

## Technologies
* [Docker](https://www.docker.com)
* [Google Cloud Command-line Tool](https://cloud.google.com/sdk/gcloud)
* [Google App Engine Flexible Environment](https://cloud.google.com/appengine/docs/flexible)
* [Google Cloud Platform](https://cloud.google.com)
* [Google Datastore](https://cloud.google.com/datastore)
* [JDK 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven](https://maven.apache.org)
* [Spring Boot](https://spring.io/projects/spring-boot)

## Architecture

![Architecture Design](images/architecture.png)

### Google Cloud Platform
GCP is a portfolio of computing solutions for storage, compute capacity, Big Data and Machine Learning.  

* App Engine  
A platform for building scalable backend applications.

* Cloud SQL  
Store and manage data using a fully-managed, relational MySQL database.

* BigQuery  
A fast, economical, and fully-managed data warehouse for large-scale analytics.

* Cloud Translation API  
Create multilingual apps and translate text into other languages programmatically.

* Container Engine  
Run Docker containers on Google Cloud infrastructure, orchestrated by Kubernetes.

* Cloud Machine Learning Engine  
Fast, large scale, and easy-to-use Machine Learning services.

### App Engine Flexible Environment
App Engine is based on Google Compute Engine, the App Engine flexible environment automatically scales your app up and down while balancing the load. 
Microservices, authorization, SQL and NoSQL databases, traffic splitting, logging, versioning, security scanning, and content delivery networks are all supported natively. 
In addition, the App Engine flexible environment allows you to customize the runtime and even the operating system of your virtual machine using Dockerfiles.

* Runtimes - The flexible environment includes native support for Java 8 (with no web-serving framework), Eclipse Jetty 9, Python 2.7 and Python 3.6, Node.js, Ruby, PHP, .NET core, 
and Go. Developers can customize these runtimes or provide their own runtime by supplying a custom Docker image or Dockerfile from the open source community.

* Infrastructure Customization - Because VM instances in the flexible environment are Google Compute Engine virtual machines, 
you can take advantage of custom libraries, use SSH for debugging, and deploy your own Docker containers.

* Performance - Take advantage of a wide array of CPU and memory configurations. 
You can specify how much CPU and memory each instance of your application needs and the flexible environment will provision the necessary infrastructure for you.

App Engine manages your virtual machines, ensuring that:

* Instances are health-checked, healed as necessary, and co-located with other services within the project.
* Critical, backwards compatible updates are automatically applied to the underlying operating system.
VM instances are automatically located by geographical region according to the settings in your project. 
Google's management services ensure that all of a project's VM instances are co-located for optimal performance.
* VM instances are restarted on a weekly basis. During restarts Google's management services will apply any necessary operating system and security updates.
* You always have root access to Compute Engine VM instances. SSH access to VM instances in the flexible environment is disabled by default. 
If you choose, you can enable root access to your app's VM instances.

[See more details about the differences between the standard environment and the flexible environment.](https://cloud.google.com/appengine/docs/the-appengine-environments)

This application will run on a Docker container with default configuration as this project does not declare a Docker file to customize it.  
It is also possible to orchestrated application's instances with Kubernetes Engine as described on this [how-to guide](https://cloud.google.com/appengine/docs/flexible/java/run-flex-app-on-kubernetes).

### Datastore
Google Cloud Datastore is a NoSQL document database built for automatic scaling, high performance, and ease of application development.  
Cloud Datastore features include:  

* Atomic transactions: Cloud Datastore can execute a set of operations where either all succeed, or none occur.  
* High availability of reads and writes: Cloud Datastore runs in Google data centers, which use redundancy to minimize impact from points of failure.  
* Massive scalability with high performance: Cloud Datastore uses a distributed architecture to automatically manage scaling. 
Cloud Datastore uses a mix of indexes and query constraints so your queries scale with the size of your result set, not the size of your data set.  
* Flexible storage and querying of data: Cloud Datastore maps naturally to object-oriented and scripting languages, and is exposed to applications through multiple clients. 
It also provides a SQL-like query language.  
* Balance of strong and eventual consistency: Cloud Datastore ensures that entity lookups by key and ancestor queries always receive strongly consistent data. 
All other queries are eventually consistent. The consistency models allow your application to deliver a great user experience while handling large amounts of data and users.  
* Encryption at rest: Cloud Datastore automatically encrypts all data before it is written to disk and automatically decrypts the data when read by an authorized user. 
For more information, see Server-Side Encryption.  
* Fully managed with no planned downtime: Google handles the administration of the Cloud Datastore service so you can focus on your application. 
Your application can still use Cloud Datastore when the service receives a planned upgrade.  

App Engine uses indexes in Cloud Datastore for every query your application makes. 
These indexes are updated whenever an entity changes, so the results can be returned quickly when the app makes a query. 
To do this, Cloud Datastore needs to know in advance which queries the application will make. 
You specify which indexes your app needs in a configuration file. 
The local development server can automatically generate the index configuration file as you test your app.

Every Cloud Datastore query made by an application needs a corresponding index. 
Indexes for simple queries, such as queries over a single property, are created automatically. 
Indexes for complex queries must be defined in a configuration file named index.yaml. 
This file is deployed with your application to create the indexes in Cloud Datastore.

### Docker
Docker is a tool designed to make it easier to create, deploy, and run applications by using containers. 
Containers allow a developer to package up an application with all of the parts it needs, such as libraries and other dependencies, and ship it all out as one package. 
By doing so, thanks to the container, the developer can rest assured that the application will run on any other Linux machine regardless of 
any customized settings that machine might have that could differ from the machine used for writing and testing the code.  

In a way, Docker is a bit like a virtual machine. But unlike a virtual machine, rather than creating a whole virtual operating system, Docker allows applications to use the same 
Linux kernel as the system that they're running on and only requires applications be shipped with things not already running on the host computer. 
This gives a significant performance boost and reduces the size of the application.

### Spring Boot  
Spring Boot makes it easy to create standalone, production-grade Spring based Applications that you can "just run". 
The framework takes an opinionated view of the Spring platform and third-party libraries so you can get started with minimum fuss. 
Most Spring Boot applications need very little Spring configuration.  

The goal of Spring Boot is to provide a set of tools for quickly building Spring applications that are easy to configure. 
It is a modular application to abstract the configuration required for integrate all the modules. 
All you have to do is to reference the starter on the Maven or Gradle configuration file.  

If you've ever written a Spring-based application, you know that a lot of work goes into configuring it just to get to "Hello World". 
This isn't a bad thing: Spring is an elegant set of frameworks that require carefully coordinated configuration to work correctly. 
But that elegance comes at the cost of configuration complexity.  

Starters are a big part of the magic of Spring Boot, used to limit the amount of manual dependency configuration that you have to do. 
If you're going to use Spring Boot effectively, you should know about starters. 
A starter is essentially a set of dependencies (such as a Maven POM) that are specific to the type of application the starter represents.

### Restaurant Backoffice Application
This application is segmented in two modules, one is Spring Boot standalone application and the another aims to make the interaction with Datastore transparent.

* rest-api - Spring Boot standalone application designed to be deployed on Google App Engine Flexible Environment.  
	The [app.yaml](rest-api/src/main/appengine/app.yaml) file describes an application's deployment configuration for GAE (Google App Engine).  
	It exposes a RESTFul API under the path /restaurants and its specification is available at [Swagger UI](http://localhost:8080/swagger-ui.html).  
	Features:  
	- Create, Update, Delete and List Restaurants on Datastore  
	- Create, Update, Delete and List Menu Items of a Restaurant on Datastore  
* datastore  
	Implements a simple architecture to integrates with Google Datastore easily through google-cloud-datastore API.   
	The framework [objectify](https://github.com/objectify/objectify) was the first option, but it isn't working as expected on GAE Flexible Environment.    
	The [index.yaml](rest-api/index.yaml) file defines application required indexes needed to run some queries.

## Running Locally
* Connect on Datastore (choose one of two options in below):
	- Emulate Datastore: `gcloud beta emulators datastore start --host-port=localhost:[port]` and `set DATASTORE_EMULATOR_HOST=localhost:[port]`
	- Authenticate on Google Cloud Datastore: `gcloud beta auth application-default login`  
	Local application will persist and query data from Cloud Datastore.  
	A project should be created on GCP at this step to go with this option, more detail on (via [Google Cloud Console](https://cloud.google.com/resource-manager/docs/creating-managing-projects) 
	or [gcloud](https://cloud.google.com/sdk/gcloud/reference/projects/create) Command-line Tool). 
* Install: `mvn clean install`
* Set [Jasypt](https://github.com/ulisesbocchio/jasypt-spring-boot) encryptor password as a environment variable, this step must be done before running the application.
    - Windows: `set JASYPT_ENCRYPTOR_PASSWORD=Backoffice@2018`
    - Unix: `export JASYPT_ENCRYPTOR_PASSWORD=Backoffice@2018`
Detail: It can be pass as an execution argument too: `-Djasypt.encryptor.password=Backoffice@2018`
* Run: Go to rest-api directory and run: `mvn spring-boot:run` or `java -jar target/rest-api-1.0.0.jar`

## Deploying on Google App Engine
* Run the following command on project root directory to create the binaries: `mvn clean install`
* If you haven't created project on GPC yet, create it via [Google Cloud Console](https://cloud.google.com/resource-manager/docs/creating-managing-projects) or [gcloud](https://cloud.google.com/sdk/gcloud/reference/projects/create) command-line tool
* Run the following command on project root directory to create the required indexes on GCP: `gcloud app deploy index.yaml`
* The indexes must be created on [Datastore Indexes](https://console.cloud.google.com/datastore/indexes)
* Run the following command on rest-api directory to deploy restaurant-backoffice application on GAE: `mvn appengine:deploy`
* If previous step was executed with success, a new version will appear on [Versions](https://console.cloud.google.com/appengine/versions) page similar to the one presented on below snapshot.  
	Click on version link highlighted on below image to access the running application. User/Password: backoffice_user/GAE@2018  
	![Architecture Design](images/versions.png)  
	Version page screenshot  

	Swagger UI is present at /swagger-ui.html.  
	![Architecture Design](images/swagger-ui.png)  
	Swagger UI screenshot  