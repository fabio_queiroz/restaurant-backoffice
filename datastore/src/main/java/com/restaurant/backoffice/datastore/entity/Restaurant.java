package com.restaurant.backoffice.datastore.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * Restaurant entity
 */
@ApiModel(
        value="Restaurant",
        description="Restaurant Entity model"
)
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Restaurant {

    /** Restaurant id */
    @ApiModelProperty("Restaurant id")
    private Long id;

    /** Restaurant name */
    @ApiModelProperty("Restaurant name")
    @NotEmpty(message = "Restaurant name is required")
    private String name;

}
