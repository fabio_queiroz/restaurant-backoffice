package com.restaurant.backoffice.datastore;


import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.DatastoreOptions;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Datastore Configuration
 */
@Configuration
public class DatastoreConfig {

    /**
     * Define Datastore bean
     *
     * @return Datastore bean
     */
    @Bean
    public Datastore datastore() {
        return DatastoreOptions.getDefaultInstance().getService();
    }

}
