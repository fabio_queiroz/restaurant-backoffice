package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.restaurant.backoffice.datastore.entity.MenuItem;
import com.restaurant.backoffice.datastore.entity.Restaurant;
import org.springframework.stereotype.Repository;

/**
 * Restaurant repository implementation using Datastore API
 */
@Repository
public class RestaurantDatastoreRepository extends AbstractEntityDatastoreRepository<Restaurant>
        implements RestaurantRepository {

    private final MenuItemDatastoreRepository menuItemDatastoreRepository;

    /**
     * Default constructor
     *
     * @param datastore Datastore
     * @param menuItemDatastoreRepository
     */
    public RestaurantDatastoreRepository(
            Datastore datastore,
            MenuItemDatastoreRepository menuItemDatastoreRepository) {
        super(datastore);
        this.menuItemDatastoreRepository = menuItemDatastoreRepository;
    }

    /**
     * Creates a restaurant bean
     *
     * @param entity Entity
     * @return Restaurant
     */
    protected Restaurant newEntityBean(Entity entity) {
        return Restaurant.builder()
                .id(entity.getKey().getId())
                .name(entity.getString("name")).build();
    }

    /**
     * Creates Datastore entity
     *
     * @param restaurant Restaurant
     * @return Datastore entity
     */
    @Override
    protected Entity newEntity(Restaurant restaurant) {
        return Entity.newBuilder(key(restaurant.getId()))
                .set("name", restaurant.getName())
                .build();
    }

    /**
     * Delete restaurant and its menu items
     *
     * @param restaurantId Restaurant id
     */
    @Override
    public void delete(long restaurantId) {
        menuItemDatastoreRepository.findAllByRestaurantId(restaurantId).stream()
                .mapToLong(MenuItem::getId).forEach(menuItemDatastoreRepository::delete);
        super.delete(restaurantId);
    }

}
