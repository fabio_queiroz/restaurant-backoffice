package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItem;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Menu Item repository implementation using Datastore API
 */
@Repository
public class MenuItemDatastoreRepository extends AbstractEntityDatastoreRepository<MenuItem>
        implements MenuItemRepository {

    /**
     * Default constructor
     *
     * @param datastore Datastore
     */
    public MenuItemDatastoreRepository(Datastore datastore) {
        super(datastore);
    }

    /**
     * Creates a Menu Item bean
     *
     * @param entity Entity
     * @return Menu Item
     */
    protected MenuItem newEntityBean(Entity entity) {
        return MenuItem.builder()
                .id(entity.getKey().getId())
                .name(entity.getString("name"))
                .price(entity.getDouble("price"))
                .restaurantId(entity.getLong("restaurantId"))
                .build();
    }

    /**
     * Creates Datastore entity
     *
     * @param menuItem Menu Item
     * @return Datastore entity
     */
    @Override
    protected Entity newEntity(MenuItem menuItem) {
        return Entity.newBuilder(key(menuItem.getId()))
                .set("name", menuItem.getName())
                .set("price", menuItem.getPrice())
                .set("restaurantId", menuItem.getRestaurantId())
                .build();
    }

    /**
     * Find all Menu Items of a Restaurant
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItem> findAllByRestaurantId(long restaurantId) {
        return runQuery(
            queryBuilder()
            .setFilter(
                StructuredQuery.CompositeFilter.and(
                    StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId)
                )
            )
        );
    }

    /**
     * Find all Menu Items of a Restaurant sorted
     *
     * @param restaurantId Restaurant id
     * @param orderBy Order by
     *
     * @return List of Menu Items
     */
    @Override
    public List<MenuItem> findAllByRestaurantId(long restaurantId, StructuredQuery.OrderBy orderBy) {
        return runQuery(
            queryBuilder(orderBy)
            .setFilter(
                StructuredQuery.CompositeFilter.and(
                    StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId)
                )
            )
        );
    }

}
