package com.restaurant.backoffice.datastore.repository;

import com.restaurant.backoffice.datastore.entity.Restaurant;

/**
 * Restaurant repository
 */
public interface RestaurantRepository extends EntityRepository<Restaurant> {

}
