package com.restaurant.backoffice.datastore.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

/**
 * Menu item entity
 */
@ApiModel(
        value="MenuItem",
        description="Menu Item Entity model"
)
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuItem {

    /** Menu Item id */
    @ApiModelProperty("Menu Item id")
    private Long id;

    /** Menu Item name */
    @ApiModelProperty("Menu Item name")
    @NotEmpty(message = "Menu Item name is required")
    private String name;

    /** Menu Item price */
    @ApiModelProperty("Menu Item price")
    @Positive(message = "Menu Item price must be positive")
    private double price;

    /** Restaurant id */
    @ApiModelProperty("Restaurant id")
    @JsonIgnore
    private Long restaurantId;

}
