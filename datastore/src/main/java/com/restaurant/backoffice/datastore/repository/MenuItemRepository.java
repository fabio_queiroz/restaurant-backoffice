package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItem;

import java.util.List;

/**
 * Menu Item repository
 */
public interface MenuItemRepository extends EntityRepository<MenuItem> {

    /**
     * Find all Menu Items of a Restaurant
     *
     * @param restaurantId Restaurant id
     *
     * @return List of Menu Items
     */
    List<MenuItem> findAllByRestaurantId(long restaurantId);

    /**
     * Find all Menu Items of a Restaurant
     *
     * @param restaurantId Restaurant id
     * @param orderBy Order by
     *
     * @return List of Menu Items
     */
    List<MenuItem> findAllByRestaurantId(long restaurantId, StructuredQuery.OrderBy orderBy);

}
