package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Entity;
import com.google.cloud.datastore.Key;
import com.google.cloud.datastore.QueryResults;
import com.google.cloud.datastore.StructuredQuery;
import com.restaurant.backoffice.datastore.entity.MenuItem;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MenuItemDatastoreRepositoryTest extends AbstractDatastoreRepositoryTest {

    private final long restaurantId1 = 1;
    private final long id1 = 1;
    private final long id2 = 2;

    private MenuItem menuItem1;
    private MenuItem menuItem2;
    private Entity entity1;
    private Entity entity2;

    private MenuItemDatastoreRepository menuItemRepository;

    @Mock
    private Key key1;

    @Mock
    private Key key2;

    @Override
    protected String getKind() {
        return MenuItem.class.getSimpleName();
    }

    public void before() {
        when(keyFactory.newKey(id1)).thenReturn(key1);
        //when(keyFactory.newKey(id2)).thenReturn(key2);
        when(key1.getId()).thenReturn(id1);
        when(key2.getId()).thenReturn(id2);
        menuItemRepository = new MenuItemDatastoreRepository(datastore);
        menuItem1 = MenuItem.builder()
                .id(id1)
                .name("Item 1")
                .price(10.50)
                .restaurantId(restaurantId1)
                .build();
        entity1 = Entity.newBuilder(key1)
                .set("name", menuItem1.getName())
                .set("price", menuItem1.getPrice())
                .set("restaurantId", menuItem1.getRestaurantId())
                .build();
        menuItem2 = MenuItem.builder()
                .id(id2)
                .name("Item 2")
                .price(20.50)
                .restaurantId(restaurantId1)
                .build();
        entity2 = Entity.newBuilder(key2)
                .set("name", menuItem2.getName())
                .set("price", menuItem2.getPrice())
                .set("restaurantId", menuItem2.getRestaurantId())
                .build();
    }

    public void after() {

    }

    @Test
    public void findById_Not_Found() {
        Optional<MenuItem> menuItem = menuItemRepository.findById(id1);
        assertFalse(menuItem.isPresent());
    }

    @Test
    public void findById() {
        when(datastore.get(key1)).thenReturn(entity1);
        Optional<MenuItem> menuItemOpt = menuItemRepository.findById(id1);
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).get(key1);
        assertEquals(menuItem1, menuItemOpt.get());
    }

    @Test
    public void findAll() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity2).thenReturn(entity1);
        when(datastore.run(menuItemRepository.queryBuilder().build())).thenReturn(queryResults);
        List<MenuItem> items = menuItemRepository.findAll();
        assertEquals(Arrays.asList(menuItem2, menuItem1), items);
        verify(datastore, times(1)).run(menuItemRepository.queryBuilder().build());
    }

    @Test
    public void findAll_Sorted() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity1, entity2);
        when(datastore.run(menuItemRepository.queryBuilder(orderByName).build())).thenReturn(queryResults);
        List<MenuItem> items = menuItemRepository.findAll(orderByName);
        assertEquals(Arrays.asList(menuItem1, menuItem2), items);
        verify(datastore, times(1)).run(menuItemRepository.queryBuilder(orderByName).build());
    }

    @Test
    public void findAllByRestaurantId() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity2).thenReturn(entity1);
        when(datastore.run(menuItemRepository.queryBuilder()
                .setFilter(
                        StructuredQuery.CompositeFilter.and(
                                StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId1)
                        )
                ).build())).thenReturn(queryResults);
        List<MenuItem> items = menuItemRepository.findAllByRestaurantId(restaurantId1);
        assertEquals(Arrays.asList(menuItem2, menuItem1), items);
        verify(datastore, times(1)).run(menuItemRepository.queryBuilder()
                .setFilter(
                        StructuredQuery.CompositeFilter.and(
                                StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId1)
                        )
                ).build());
    }

    @Test
    public void findAllByRestaurantId_Sorted() {
        QueryResults<Entity> queryResults = mock(QueryResults.class);
        when(queryResults.hasNext()).thenReturn(true).thenReturn(true).thenReturn(false);
        when(queryResults.next()).thenReturn(entity1).thenReturn(entity2);
        when(datastore.run(menuItemRepository.queryBuilder(orderByName)
                .setFilter(
                        StructuredQuery.CompositeFilter.and(
                                StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId1)
                        )
                ).build())).thenReturn(queryResults);
        List<MenuItem> items = menuItemRepository.findAllByRestaurantId(restaurantId1, orderByName);
        assertEquals(Arrays.asList(menuItem1, menuItem2), items);
        verify(datastore, times(1)).run(menuItemRepository.queryBuilder(orderByName)
                .setFilter(
                        StructuredQuery.CompositeFilter.and(
                                StructuredQuery.PropertyFilter.eq("restaurantId", restaurantId1)
                        )
                ).build());
    }

    @Test
    public void save_Create() {
        long id3 = 3;
        MenuItem menuItem3 = MenuItem.builder()
                .name("Item 3")
                .price(30.50)
                .restaurantId(restaurantId1)
                .build();
        Key key3 = mock(Key.class);
        when(key3.getId()).thenReturn(id3);
        when(keyFactory.newKey()).thenReturn(key3);
        when(datastore.allocateId(key3)).thenReturn(key3);
        long id = menuItemRepository.save(menuItem3);
        assertEquals(id3, id);
        verify(keyFactory, times(1)).newKey();
        verify(datastore, times(1)).allocateId(key3);
        verify(datastore, times(1)).put(
                Entity.newBuilder(key3)
                        .set("name", menuItem3.getName())
                        .set("price", menuItem3.getPrice())
                        .set("restaurantId", menuItem3.getRestaurantId())
                        .build()
        );
    }

    @Test
    public void save_Update() {
        MenuItem menuItem1_1 = MenuItem.builder()
                .id(id1)
                .name("Item 1_1")
                .price(10.50)
                .restaurantId(restaurantId1)
                .build();
        when(keyFactory.newKey(id1)).thenReturn(key1);
        long id = menuItemRepository.save(menuItem1_1);
        assertEquals(id1, id);
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).put(
                Entity.newBuilder(key1)
                        .set("name", menuItem1_1.getName())
                        .set("price", menuItem1_1.getPrice())
                        .set("restaurantId", menuItem1_1.getRestaurantId())
                        .build()
        );
    }

    @Test
    public void delete() {
        menuItemRepository.delete(id1);
        verify(keyFactory, times(1)).newKey(id1);
        verify(datastore, times(1)).delete(key1);
    }

}