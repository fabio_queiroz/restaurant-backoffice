package com.restaurant.backoffice.datastore.repository;

import com.google.cloud.datastore.Datastore;
import com.google.cloud.datastore.KeyFactory;
import com.google.cloud.datastore.StructuredQuery;
import org.junit.After;
import org.junit.Before;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public abstract class AbstractDatastoreRepositoryTest {

    protected final StructuredQuery.OrderBy orderByName = StructuredQuery.OrderBy.asc("name");

    protected Datastore datastore;

    protected KeyFactory keyFactory;

    protected abstract void before();

    protected abstract void after();

    protected abstract String getKind();

    @Before
    public void setUp() {
        datastore = mock(Datastore.class);
        keyFactory = mock(KeyFactory.class);
        when(datastore.newKeyFactory()).thenReturn(keyFactory);
        when(keyFactory.setKind(getKind())).thenReturn(keyFactory);
        before();
    }

    @After
    public void tearDown() {
        verify(keyFactory, times(1)).setKind(getKind());
        after();
    }

}